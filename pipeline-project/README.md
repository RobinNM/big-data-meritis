# Pipeline Project 
This repository contains the different ressources concerning the project of a Big Data pipeline with the goal of treating in real time the data proposed by the RATP API

# API RATP :

## Useful links
* API SOAP Page : https://data.ratp.fr/page/temps-reel/
* API REST Doc : 
    * https://help.opendatasoft.com/apis/ods-search-v1/#available-apis
    * https://data.ratp.fr/api/v1/console/datasets/1.0/search/
* Exploration des données : https://data.ratp.fr/explore/?sort=modified
* Situation géographique des gares : https://data.ratp.fr/explore/dataset/positions-geographiques-des-stations-du-reseau-ratp/information/?disjunctive.stop_name

## SOAP API Details 

**Réseau** : 
* Ensemble de ligne tel qu'une ligne n'appartienne qu'à un seul réseau
* Equivalent de réseau Métro, RER, bus, ...
* **Exemples :** 
	* metro 	Métro
	* rer 	RER
	* busratp 	Bus RATP
	* tram 	Tramway
	* noctilienratp 	Noctilien RATP
	* noctiliensncf 	Noctilien SNCF
	* sncf 	SNCF Transilien
	* (eg. optile)	(eg. Optile)

**Réseau ou Group** :
* Groupement de Réseau
* Une ligne appartient a N groupement de réseau
* **Exemples :** 
	* bus 	busratp + noctilien* + optile + autres bus  <=> Tous sauf ferretram (metro,rer,sncf,tram).
	* ferretram 	metro + rer + sncf + tram <=> Tous sauf bus.
	* bustram 	bus + tram <=> Tous sauf ferre (metro,rer,sncf).
	* ferre 	metro + rer + sncf <=> Tous sauf bus et tram.
	* noctilien 	noctilienratp + noctiliensncf
	* bustramjour 	bus + tram - noctilien

**Ligne :**
* Trajet, itinéraire régulier emprunté par des missions 
* Elle fait partie __d'un__ réseau.
* Elle dessert __plusieurs__ stations.
* Elle peut avoir __deux__ directions ou __une__ (ligne circulaire)

**Direction :**
* Sens, direction générale (Aller ou Retour - Est ou Ouest - Coté de ligne - ...) 
* Son sens l'identifie et a pour valeur "A" ou "R" 
* Elle peut avoir une fin de ligne ou plusieurs (cas des lignes à fourche).
* Son nom reprend en général le nom de toutes ses fins de ligne.

**Station :**  
* Elle est rattachée à une ligne.
* Dans le cas d'une boucle, elle n'est présente que dans une direction (eg. 7bis Danube).

**Mission :**
* Course 
* Elle est liée à une direction et plus précisément à une fin de ligne
* Elle a un terminus qui est la dernière station desservie de sa liste de stations.

Infos taken from the development kit. 
I recommend using the python library **zeep** to interect with the SOAP API

# Qualité de l'air : 

* DATE: date[text]
* HEURE: heure[text]
* NO: nocha4[text] Unité = µg.m-3
* NO2: n2cha4[text] Unité = µg.m-3
* PM10: 10cha4[text] Unité = µg.m-3
* CO2: c2cha4[text] Unité = ppm
* TEMP: tcha4[double] Température Unité = degré
* HUMI: hycha4[double] Humidité relative Unité = %

# Kafka : 

Tutorial for setting up a Kafka node on a computer (linux) : 

https://tecadmin.net/install-apache-kafka-ubuntu/

